#include <errno.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include <linux/ethtool.h>
#include <linux/sockios.h>
#include <net/if.h>
#include <sys/ioctl.h>
#include <sys/socket.h>

void help(void) {
    printf("Expected -d <devname> -t <polling period in ms>\n");
    printf("This application acts similar to ethtool -S <dev>\n");
    printf("but does so in a loop. Piping the output to /dev/null is advised\n");
}


// Lifted from ethtool:
// https://github.com/Distrotech/ethtool/blob/distrotech-ethtool/ethtool.c#L2009
// Freeing and allocating memory like this isn't really ideal in a loop scenario
// and could be improved
static int do_gstats(int fd, struct ifreq *ifr)
{
	struct ethtool_drvinfo drvinfo;
	struct ethtool_gstrings *strings;
	struct ethtool_stats *stats;
	unsigned int n_stats, sz_str, sz_stats, i;
	int err;

	drvinfo.cmd = ETHTOOL_GDRVINFO;
	ifr->ifr_data = (caddr_t)&drvinfo;
	err = ioctl(fd, SIOCETHTOOL, ifr);
	if (err < 0) {
		perror("Cannot get driver information");
		return 71;
	}

	n_stats = drvinfo.n_stats;
	if (n_stats < 1) {
		fprintf(stderr, "no stats available\n");
		return 94;
	}

	sz_str = n_stats * ETH_GSTRING_LEN;
	sz_stats = n_stats * sizeof(uint64_t);

	strings = calloc(1, sz_str + sizeof(struct ethtool_gstrings));
	stats = calloc(1, sz_stats + sizeof(struct ethtool_stats));
	if (!strings || !stats) {
		fprintf(stderr, "no memory available\n");
		return 95;
	}

	strings->cmd = ETHTOOL_GSTRINGS;
	strings->string_set = ETH_SS_STATS;
	strings->len = n_stats;
	ifr->ifr_data = (caddr_t) strings;
	err = ioctl(fd, SIOCETHTOOL, ifr);
	if (err < 0) {
		perror("Cannot get stats strings information");
		free(strings);
		free(stats);
		return 96;
	}

	stats->cmd = ETHTOOL_GSTATS;
	stats->n_stats = n_stats;
	ifr->ifr_data = (caddr_t) stats;
	err = ioctl(fd, SIOCETHTOOL, ifr);
	if (err < 0) {
		perror("Cannot get stats information");
		free(strings);
		free(stats);
		return 97;
	}

	/* todo - pretty-print the strings per-driver */
	fprintf(stdout, "NIC statistics:\n");
	for (i = 0; i < n_stats; i++) {
		fprintf(stdout, "     %.*s: %llu\n",
			ETH_GSTRING_LEN,
			&strings->data[i * ETH_GSTRING_LEN],
			stats->data[i]);
	}
	free(strings);
	free(stats);

	return 0;
}

int main(int argc, char **argv) {
    char *devname = NULL;
    int period_ms = 0;
    int fd;
    struct timespec next_time;
    struct ifreq if_req = {};

    if (argc != 5) {
        help();
        return 1;
    }

    for (int i = 0; i < argc; ++i) {
        if (strcmp("-d", argv[i]) == 0) {
            devname = argv[i + 1];
        }

        if (strcmp("-t", argv[i]) == 0) {
            period_ms = atoi(argv[i + 1]);
        }
    }

    if (!devname || !period_ms) {
        return 1;
    }

    strcpy(if_req.ifr_name, devname);
    fd = socket(AF_INET, SOCK_DGRAM, 0);


    /* Initalize next_time */
    if (clock_gettime(CLOCK_MONOTONIC, &next_time) < 0) {
        perror("Failed to get time: \n");
    }

    while (1) {
        /* Calculate next period to wake up */
        next_time.tv_nsec += period_ms * 1000000;
        if (next_time.tv_nsec >= 1000000000) {
            /* Overflow case */
            next_time.tv_sec++;
            next_time.tv_nsec -= 1000000000;
        }

        /* Do work */
        do_gstats(fd, &if_req);

        /* Sleep until the next period */
        if(clock_nanosleep(CLOCK_MONOTONIC, TIMER_ABSTIME, &next_time, NULL)) {
            perror("clock_nanosleep() failed:\n");
        }
    }

    return 0;
}
